/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#include "hlx_server.hpp"

int main(int argc, char *argv[])
{
	int ret = -1;
	ChildAServer *ca = NULL;

	if (argc != 4) {
		ffloge("failed to start childA, argc need 4");
		goto finally;
	}

	if ((ca = new ChildAServer) == NULL) {
		ffloge("failed to new ChildAServer");
		goto finally;
	}

	if (ca->init(argv[1], atoi(argv[2]), atoi(argv[3]))) {
		fflog("failed to ChildAServer->init");
		goto finally;
	}

	if (ca->start()) {
		fflog("failed to ChildAServer->start");
		goto finally;
	}

	ret = 0;
finally:
	DO_DELETE(ca);
	return ret;

}
