/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#include "hlx_client.hpp"

int hlx_exit_byte = -1;

/************************************
 * HlxClient 
 ***********************************/
HlxClient::~HlxClient() {
};

const char *HlxClient::exec_name()
{
	return argv[0];
}

const char **HlxClient::argvs()
{
	return argv;
}

int HlxClient::stop()
{
	//push EXIT byte if process is running
	if (_status == ChildProcessIsRunning) {
		//don't care the return status
		push(&hlx_exit_byte);
	}
	return ChildProcess::stop();
}

int HlxClient::start() {
	int ret;

	if ((ret = ChildProcess::start()) == ChildProcessIsRunning) {
		log("Process Created");
	}

	return ret;
}

/************************************
 * ChildAClient 
 ***********************************/
ChildAClient::~ChildAClient() {
	ChildProcess::stop();
	if (_fd[0] != -1) {
		close(_fd[0]);
	}
	if (_fd[1] != -1) {
		close(_fd[1]);
	}
}

int ChildAClient::init(string name)
{
	Module::init(name);
	
	if (pipe(_fd)) {
		loge("failed to create pipe");
		return -1;
	}

	_pipe_0 = std::to_string(_fd[0]);
	_pipe_1 = std::to_string(_fd[1]);

	argv[0] = "childA";
	argv[1] = "[childA]";
	argv[2] = _pipe_0.c_str();
	argv[3] = _pipe_1.c_str();
	argv[4] = NULL;

	//log("_fd[0]: %d _fd[1]:%d", _fd[0], _fd[1]);
	return 0;
}

int ChildAClient::push(void *data)
{
	ssize_t ret;

	if (ChildProcess::status(1)) {
		loge("child process has been leaved");
		return -1;
	}

	if ((ret = write(_fd[1], data, 4)) != 4) {
		loge("failed to write %s", strerror(errno));
		return -1;
	}

	return 0;
}


/************************************
 * ChildBClient 
 ***********************************/
ChildBClient::~ChildBClient() {
	ChildProcess::stop();
	DO_DELETE(_shm_buf);
}

int ChildBClient::init(string name)
{
	Module::init(name);
	argv[0] = "childB";
	argv[1] = "[childB]";
	argv[2] = NULL;

	if ((_shm_buf = new SemShmRingBuf(SEM_NAME, SHM_PATH, RING_BUF_SIZE, RING_BUF_NUM, 1)) == NULL) {
		loge("failed to new SemShmRingBuf");
		return -1;
	}

	if (_shm_buf->init()) {
		loge("failed to _shm_buf->init()");
		return -1;
	}
	return 0;
}

int ChildBClient::push(void *data)
{
	if (ChildProcess::status(1)) {
		loge("child process has been leaved");
		return -1;
	}

	if (_shm_buf) {
		return _shm_buf->push(data);
	}
	return -1;
}

/************************************
 * mainParent 
 ***********************************/
mainParent::~mainParent() {
//	stop();
	DO_DELETE(_childa);
	DO_DELETE(_childb);
}

int mainParent::init(string name)
{
	int ret = -1;
	Module::init(name);
	
	if ((_childa = new ChildAClient) == NULL) {
		loge("failed to new ChildAClient");
		goto finally;
	}

	if (_childa->init(_name+" "+string("childA"))) {
		log("failed to ChildBClient->init");
		goto finally;
	}
	
	if ((_childb = new ChildBClient) == NULL) {
		loge("failed to new ChildBClient");
		goto finally;
	}

	if (_childb->init(_name+" "+string("childB"))) {
		log("failed to ChildBClient->init");
		goto finally;
	}

	ret = 0;
finally:
	return ret;
}

int mainParent::do_get_num()
{
	string str = "";
	int is_negative = 0;
	int is_real = 0;


	logln();
	logln();
	log_noline("Enter a positive integer or 0 to exit: ");
	str = get_number_str(&is_negative, &is_real);
	logln();

	if (is_negative) {
		log("Can only accept positive integers, please try again.");
		return -1;
	}

	if (is_real) {
		log("Can only accept positive integers, please try again.");
		return -1;
	}

	return atoi(str.c_str());
}

int mainParent::generate_num_and_feed(int num)
{
	int ret = -1;
	int i;
	int_vector vec;

	log_noline("Generating %d random integers: ", num);
	for (i=0; i<num; i++) {
		vec.push_back(rnd(50, 100));
	}

	PRT_VEC(vec);

	//send total number
	if (_childa->push(&num)) {
		loge("failed to childA->push");
		goto finally;
	}

	if (_childb->push(&num)) {
		loge("failed to childB->push");
		goto finally;
	}

	//send number in vector one by one
	for (int i: vec) {
		if (_childa->push(&i)) {
			loge("failed to childB->push");
			goto finally;
		}

		if (_childb->push(&i)) {
			loge("failed to childB->push");
			goto finally;
		}
	}

	ret = 0;
finally:
	return ret;
}

int mainParent::start()
{
	int num;

	log("Main Process Started");
	
	if (_childa->start()) {
		loge("failed to start childA process");
		goto error;
	}

	if (_childb->start()) {
		loge("failed to start childB process");
		goto error;
	}

	do {
		//workaround: sleep 1ms for waiting client to received, calc and print, otherwise the screen will broken
		usleep(1000);

		if ((num = do_get_num()) == -1) {
			continue;
		}

		if (num == 0) {
			break;
		}

		if (generate_num_and_feed(num)) {
			break;
		}
	} while(1);
	
	return Module::start();
error:
	return -1;
}

int mainParent::stop()
{
	if (status() != ModuleStatusStart) {
		return -1;
	}

	log("Process Waits");
	
	if (_childa) {
		_childa->stop();
	}

	if (_childb) {
		_childb->stop();
	}

	log("Process Exits");
	return Module::stop();
}
