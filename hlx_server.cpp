/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#include "hlx_server.hpp"

/************************************
 * ChildAServer
 ***********************************/
ChildAServer::~ChildAServer()
{
	log("Child process exits");
}

int ChildAServer::init(string name, int pipe_0, int pipe_1)
{
	Module::init(name);
	_fd[0] = pipe_0;
	_fd[1] = pipe_1;
//	log("_fd[0]: %d ", _fd[0]);
//	log("_fd[0]: %d ", _fd[1]);
	return 0;
}

int ChildAServer::recive_num_and_process(int num)
{
	int i = 0;
	int val;
	ssize_t ret;
	int_vector vec;

	while(i < num) {
		if ((ret = read(_fd[0], &val, 4)) != 4) {
			loge("failed to read from fd:%d %s", _fd[0],  strerror(errno));
			break;
		}
		vec.push_back(val);
		i++;
	}

	log_noline("Random Numbers Received From Pipe: ");
	PRT_VEC(vec);

	log("Median: %f", calc_median(vec));
	return 0;
}

int ChildAServer::start()
{
	int val;
	ssize_t ret;

	Module::start();
	log("Child process started");
	
	while(1) {

		if ((ret = read(_fd[0], &val, 4)) != 4) {
			loge("failed to read from fd:%d %s", _fd[0],  strerror(errno));
			break;
		}

		if (val == hlx_exit_byte) {
			break;
		}
		
		//log("read %d", val);

		if (recive_num_and_process(val)) {
			break;
		}
	}

	return 0;
}



/************************************
 * ChildBServer
 ***********************************/
ChildBServer::~ChildBServer()
{
	DO_DELETE(_shm_buf);
	log("Child process exits");
}

int ChildBServer::init(string name)
{
	Module::init(name);
	if ((_shm_buf = new SemShmRingBuf(SEM_NAME, SHM_PATH, RING_BUF_SIZE, RING_BUF_NUM, 0)) == NULL) {
		loge("failed to new SemShmRingBuf");
		return -1;
	}
	
	if (_shm_buf->init()) {
		loge("failed to _shm_buf->init()");
		return -1;
	}
	return 0;
}

int ChildBServer::recive_num_and_process(int num)
{
	int i = 0;
	int val;
	int_vector vec;

	while(i < num) {
		if (_shm_buf->pop(&val)) {
			loge("shm read failed: %s", strerror(errno));
			return -1;
		}
		vec.push_back(val);
		i++;
	}

	log_noline("Random Numbers Received From Shared Memory: ");
	PRT_VEC(vec);

	log_noline("Sorted Sequenc: ");
	sort(vec.begin(), vec.end());
	PRT_VEC(vec);

	log("Geometric Mean: %f", calc_geometric_mean(vec));
	return 0;
}

int ChildBServer::start()
{
	int val;

	Module::start();
	log("Child process started");
	
	while(1) {
		if (_shm_buf->pop(&val)) {
			loge("shm read failed: %s", strerror(errno));
			break;
		}

		if (val == hlx_exit_byte) {
			break;
		}
		
		//log("read %d", val);

		if (recive_num_and_process(val)) {
			break;
		}
	}

	return 0;
}

