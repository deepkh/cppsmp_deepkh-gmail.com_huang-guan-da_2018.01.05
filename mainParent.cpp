/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#include "hlx_client.hpp"

int main(int argc, char *argv[])
{
	int ret = -1;
	mainParent *mp = NULL;

	if ((mp = new mainParent) == NULL) {
		ffloge("failed to new mainParent");
		goto finally;
	}

	if (mp->init("[mainParent]")) {
		fflog("failed to mainParent->init");
		goto finally;
	}

	if (mp->start()) {
		fflog("failed to mainParent->start");
		goto finally;
	}

	ret = 0;
finally:
	if (mp) {
		mp->stop();
	}
	DO_DELETE(mp);
	return ret;
}
