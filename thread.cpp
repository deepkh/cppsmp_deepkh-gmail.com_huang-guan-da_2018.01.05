/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#include "thread.hpp"

/************************************
 * thread 
 ***********************************/
Thread::~Thread() {
	if (_thd_status & 2) {
		pthread_join(_thd, 0);
	}
	if (_thd_status & 1) {
		pthread_attr_destroy(&_attr);
	}
}

Thread *Thread::create(void *(*f)(void *), void *arg) {
	int ret = -1;
	Thread *p = new Thread;

	if (pthread_attr_init(&p->_attr)) {
		ffloge("failed to init 'p->attr' %s", strerror(errno));
		goto finally;
	}

	p->_thd_status = 1;

	if (pthread_attr_setdetachstate(&p->_attr, PTHREAD_CREATE_JOINABLE)) {
		ffloge("failed to set 'attr' %s", strerror(errno));
		goto finally;
	}
	
	if ((pthread_create(&p->_thd, &p->_attr, f, NULL))) {
		ffloge("failed to init 'p->thd' %s", strerror(errno));
		goto finally;
	}

	p->_thd_status |= 2;
	ret = 0;
finally:
	if (ret) {
		DO_DELETE(p);
	}
	return p;
}

/************************************
 * Module
 ***********************************/
int Module::init(string name) {
	_name = name;
	return 0;
}

int Module::start() {
	_status = ModuleStatusStart;
	return 0;
}

int Module::stop() {
	_status = ModuleStatusStop;
	return 0;
}

int Module::status() {
	return _status;
}

#define ARGS_TO_BUF(args, fmt, _log_buf) \
	va_list args;\
	va_start(args, fmt);\
	vsnprintf(_log_buf, sizeof(_log_buf), fmt, args);\
	va_end(args);

void Module::log(const char *fmt, ...) {
	ARGS_TO_BUF(args, fmt, _log_buf);
	fflog("%s %s", _name.c_str(), _log_buf);
	fflog("\n");
}

void Module::log_noline(const char *fmt, ...)
{
	ARGS_TO_BUF(args, fmt, _log_buf);
	fflog("%s %s", _name.c_str(), _log_buf);
}

void Module::loge(const char *fmt, ...) {
	ARGS_TO_BUF(args, fmt, _log_buf);
	ffloge("%s %s", _name.c_str(), _log_buf);
}

void Module::logln() {
	fflog("\n");
}

/************************************
 * ChildProcess
 ***********************************/
int ChildProcess::start() {
	int ret;
	if (_status == ChildProcessIsRunning) {
		fflog("child process has been started");
		return -1;
	}

	if ((ret =  posix_spawn(&_pid, exec_name(), file_actions(), NULL, (char* const*)argvs(), environ)) == 0) {
		_status = ChildProcessIsRunning;
		//log("pid:%d status:%d ret:%d", _pid, _status, ret);
		return status(1);
	}
	return ret;
}

int ChildProcess::stop() {
	if (_status == ChildProcessIsRunning) {
		while(status(0) == 0) {
			sleep(1);
		}
	}
	return _status;
}

/* return < 0 if is wrong, 0 is still running */
int ChildProcess::status(int is_nonblock) {
	pid_t ret;
	int st;

	//check _pid status 
	if (_pid == 0) {
		//log("child process was stop 1");
		_status = ChildProcessWasStop;
		goto finally;
	}

	if (_pid < 0) {
		//log("child process pid %d was not > 0, something wrong!", _pid);
		_status = ChildProcessWasWrong;
		goto finally;
	}

	//child process if is still running 
	if ((ret = waitpid(_pid, &st, is_nonblock ? WNOHANG : 0)) == 0) {
		//log("child process still running");
	} else {
		if (WIFEXITED(st) && WEXITSTATUS(st) == 0) {
			//log("child process exit succeeded, status: %d\n", st);
			_status = ChildProcessWasStop;
		} else if (WIFEXITED(st) && WEXITSTATUS(st) == 127) {
			//log("child process fork error\n");
			_status = ChildProcessWasWrong;
		} else {
			//log("child process did not exit succeeded %d st:%d", ret, WEXITSTATUS(st));
			_status = ChildProcessWasWrong;
		}
		_pid = 0;
	}
finally:
	return _status;
}

int ChildProcess::status() 
{
	return ChildProcess::status(1);
}

posix_spawn_file_actions_t *ChildProcess::file_actions() {
	return NULL;
}

