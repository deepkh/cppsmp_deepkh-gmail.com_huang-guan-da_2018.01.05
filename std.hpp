/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#ifndef _STD_H_
#define _STD_H_

extern "C" {
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <inttypes.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <semaphore.h>
#include <sys/ipc.h>  
#include <sys/shm.h> 

#include <spawn.h>
#include <sys/wait.h>
extern char **environ;

#include <termios.h>
}

#include <iostream>
#include <map>
#include <list>
#include <utility>
#include <tuple>
#include <vector>
#include <algorithm>

using namespace std;

#define DO_DELETE(x) if (x) {delete x;x = NULL;}
#define DO_DELETE_ARY(x) if (x) {delete [] x;x = NULL;}
#define fflog(fmt,...) if (1) {printf(fmt,##__VA_ARGS__); }
#define ffloge(fmt,...) if (1) {fprintf(stderr, fmt, ##__VA_ARGS__);fprintf(stderr, "\n");}
#define SEM_WAIT(x) if(sem_wait(x)) {ffloge("failed to sem_wait %s", strerror(errno));}
#define SEM_POST(x) if(sem_post(x)) {ffloge("failed to sem_post %s", strerror(errno));}

typedef std::vector<int> int_vector;

int rnd(int min, int max);
double calc_geometric_mean(int_vector data);
double calc_median(int_vector data);
string get_number_str(int *is_negative, int *is_real);

#define PRT_VEC(vec) \
	for (int i: vec) { \
		fflog("%d ", i); \
	} \
	fflog("\n"); \
	fflush(stdout);
	

#endif
