/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#include "shm.hpp"

/************************************
 * Sem 
 ***********************************/
Sem::Sem(string name, int creator) 
{
	_creator = creator;
	_name = name;
}

Sem::Sem(string name) 
:Sem(name, 0)
{

}

Sem::~Sem()
{
	if (_ps) {
		sem_close(_ps);
	}
	if (_creator) {
		sem_unlink(_name.c_str());
	}
}

int Sem::open(int mod, int val)
{
	if (_creator) {
		sem_unlink(_name.c_str());
	}
	if ((_ps = sem_open(_name.c_str(), O_CREAT, mod, val)) == SEM_FAILED) {
		return errno;
	}
	return 0;
}

int Sem::wait()
{
	return sem_wait(_ps);
}

int Sem::wait_with_timeout()
{
	return wait();
	clock_gettime(CLOCK_REALTIME, &_ts);
	_ts.tv_sec += 5;
	return sem_timedwait(_ps, &_ts);
}

int Sem::post()
{
	return sem_post(_ps);
}

int Sem::val()
{
	if (sem_getvalue(_ps, &_val)) {
		ffloge("sem_getvalue err %d", errno);
	}
	return _val;
}

/************************************
 * SemMutex 
 ***********************************/
SemMutex::SemMutex(string name, int creator)
:Sem(name, creator)
{

}
SemMutex::SemMutex(string name)
:Sem(name) 
{

}

int SemMutex::open()
{
	return Sem::open(0644, 1);
}

int SemMutex::lock()
{
	return Sem::wait();
}

int SemMutex::lock_with_timeout()
{
	//return Sem::wait_with_timeout();
	return lock();
}

int SemMutex::unlock()
{
	return Sem::post();
}

/************************************
 * Shm
 ***********************************/
Shm::Shm(string path, int size, int creator)
{
	_path = path;
	_creator = creator;
	_size = size;
}

Shm::~Shm()
{
	if (_id < 0) {
		return;
	}

	if (_buf) {
		shmdt(_buf);
	}

	if (_creator) {
		shmctl(_id, IPC_RMID, NULL) ;
	}
}

int Shm::init()
{
	key_t key;
	int ret = -1;
	
	if ((key = ftok(_path.c_str(), 9)) < 0) {
		ffloge("failed to ftok %s 9", _path.c_str());
		goto finally;
	}

	if ((_id = shmget(key, _size, IPC_CREAT)) < 0) {
		//the shm already exists and also the size is not same as previous created
		//or size < SHMMIN or size > SHMMAX, so need to RMID and create new one with new size assigment
		if (errno == EINVAL) {
			fflog("the shm already exists and also the size is not same as previous created");
			fflog("or size < SHMMIN or size > SHMMAX, so need to RMID and create new one with new size assigment");
			fflog("use 'ipcs -m' to find shm_id and use 'ipcrm shm shm_id' to remove from command line");
		}

		ffloge("failed to shm_get %s 9 %s %d", _path.c_str(), strerror(errno), errno);
		goto finally;
	}

	_buf = (char *) shmat(_id, NULL, 0);
	if (((void*)_buf) == (void *)-1) {
		ffloge("failed shmat %s 9", _path.c_str());
		_buf = NULL;
		goto finally;
	}

	ret = 0;
finally:
	return ret;
}

uint8_t *Shm::buf()
{
	return (uint8_t *) _buf;
}

	


/************************************
 * SemShmRingBuf
 ***********************************/
SemShmRingBuf::SemShmRingBuf(string name, string shm_path, int size, int num, int is_producer)
{
	_size = size;
	_num = num;
	_shm = new Shm(shm_path, size*num, is_producer);
	_mutex = new SemMutex(name+"_mutex", is_producer);
	_producer = new Sem(name+"_producer", is_producer);
	_consumer = new Sem(name+"_consumer", is_producer);
}

SemShmRingBuf::~SemShmRingBuf()
{
	DO_DELETE(_consumer);
	DO_DELETE(_producer);
	DO_DELETE(_mutex);
	DO_DELETE(_shm);
}

int SemShmRingBuf::init()
{
	int ret = -1;
	if (_shm->init()) {
		goto finally;
	}

	_buf = _shm->buf();

	if (_mutex->open()) {
		ffloge("main failed to sem_open mysem");
		goto finally;
	}

	if (_producer->open(0644, _num)) {
		ffloge("main failed to sem_open mysem");
		goto finally;
	}

	if (_consumer->open(0644, 0)) {
		ffloge("main failed to sem_open mysem");
		goto finally;
	}
	ret = 0;
finally:
	return ret;
}

int SemShmRingBuf::push(void *data)
{
	if (_producer->wait_with_timeout()) {
		return -1;
	}
	if (_mutex->lock_with_timeout()) {
		return -1;
	}

	memcpy(_buf+(_producer_idx*_size), data, _size);
	_producer_idx++;
	_producer_idx%=_num;
	
	_mutex->unlock();
	_consumer->post();
	return 0;
}

int SemShmRingBuf::pop(void *data)
{
	if (_consumer->wait_with_timeout()) {
		return -1;
	}
	if (_mutex->lock_with_timeout()) {
		return -1;
	}

	memcpy(data, _buf+(_consumer_idx*_size), _size);
	_consumer_idx++;
	_consumer_idx%=_num;
	
	_mutex->unlock();
	_producer->post();
	return 0;
}

