/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#ifndef _HLX_H_
#define _HLX_H_
#include <hlx_client.hpp>

//ChildAServer (pipe) for childB process use
class ChildAServer: public Module {
protected:
	int _fd[2];
public:
	virtual ~ChildAServer();
	virtual int init(string name, int pipe_0, int pipe_1);
	virtual int start();
	virtual int recive_num_and_process(int num);
};


//ChildBServer (Share Memory) for childB process use
class ChildBServer: public Module {
protected:
	SemShmRingBuf *_shm_buf = NULL;
public:
	virtual ~ChildBServer();
	virtual int init(string name);
	virtual int start();
	virtual int recive_num_and_process(int num);
};

#endif
