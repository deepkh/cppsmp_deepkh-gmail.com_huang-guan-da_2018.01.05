/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#ifndef _SHM_H_
#define _SHM_H_
#include <std.hpp>

/************************************
 * Sem 
 ***********************************/
class Sem {
protected:
	int _creator = 0;
	sem_t *_ps = NULL;
	string _name;
	int _val;
	struct timespec _ts;
public:
	Sem(string name, int creator);
	Sem(string name);
	virtual ~Sem();
	int open(int mod, int val);
	int wait();
	int wait_with_timeout();
	int post();
	int val();
};

/************************************
 * SemMutex 
 ***********************************/
class SemMutex: public Sem {
public:
	SemMutex(string name, int creator);
	SemMutex(string name);
	int open();
	int lock();
	int lock_with_timeout();
	int unlock();
};

/************************************
 * Shm 
 ***********************************/
class Shm {
protected:
	string _path;
	int _creator = 0;
	int _size = 0;
	int _id = -1;
	char *_buf = NULL;
public:
	Shm(string path, int size, int creator);
	virtual ~Shm();
	int init();
	uint8_t *buf();
};

/************************************
 * SemShmRingBuf 
 ***********************************/
class SemShmRingBuf {
protected:
	SemMutex *_mutex = NULL;;
	Sem *_producer = NULL;
	Sem *_consumer = NULL;

	uint8_t *_buf = NULL;
	Shm *_shm = NULL;
	int _size  = 0;
	int _num = 0;

	int _producer_idx = 0;
	int _consumer_idx = 0;
public:
	SemShmRingBuf(string name, string shm_path, int size, int num, int is_producer);
	~SemShmRingBuf();
	int init();
	int push(void *data);
	int pop(void *data);
};

#endif
