/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#ifndef _HLX_CLIENT_H_
#define _HLX_CLIENT_H_
#include <std.hpp>
#include <shm.hpp>
#include <thread.hpp>

#define SEM_NAME "MYSEM"
#define SHM_PATH "/tmp"
#define RING_BUF_SIZE 4
#define RING_BUF_NUM 100

extern int hlx_exit_byte;

/************************************
 * HlxClient 
 ***********************************/
class HlxClient: public ChildProcess {
protected:
	const char *argv[10];
public:
	virtual ~HlxClient();
	virtual int stop();
	virtual int start();

	virtual const char *exec_name();
	virtual const char **argvs();
	virtual int push(void *data) = 0;
};

//ChildBClient (PIPE Version)
class ChildAClient: public HlxClient {
protected:
	string _pipe_0;
	string _pipe_1;
	int _fd[2] = {-1,-1};							//for pipe
public:
	virtual ~ChildAClient();
	virtual int init(string name);
	virtual int push(void *data);
};

//ChildBClient (Share Memory Version)
class ChildBClient: public HlxClient {
protected:
	SemShmRingBuf *_shm_buf = NULL;
public:
	virtual ~ChildBClient();
	virtual int init(string name);
	virtual int push(void *data);
};

//mainParent is aimed to management ChildAClient, ChildBClient
class mainParent: public Module {
protected:
	ChildAClient *_childa = NULL;
	ChildBClient *_childb = NULL;
public:
	virtual ~mainParent();
	virtual int init(string name);
	virtual int start();
	virtual int stop();
	virtual int do_get_num();
	virtual int generate_num_and_feed(int num);
};

#endif
