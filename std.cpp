/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#include "std.hpp"

int rnd(int min, int max)
{
	static int scount = 0;
	if (scount == 0)  {
	    srand((unsigned int)(time(NULL)*2));
		scount = 1;
	}
	return (rand()%(max-min)) + min;
}

double calc_geometric_mean(int_vector data)
{
	double m = 1.0;
	long long ex = 0;
	double invN = 1.0 / data.size();

	for (double x : data)
	{
		int i;
		double f1 = std::frexp(x,&i);
		m*=f1;
		ex+=i;
	}

	return std::pow( std::numeric_limits<double>::radix,ex * invN) * std::pow(m,invN);
}

double calc_median(int_vector data)
{
	double median;
	size_t size = data.size();

	sort(data.begin(), data.end());

	if (size  % 2 == 0) {
		median = ((double)data[size / 2 - 1] + (double)data[size / 2]) / 2.0f;
	} else  {
		median = (double)data[size / 2];
	}

	return median;
}


//ref: https://stackoverflow.com/questions/7469139/what-is-equivalent-to-getch-getche-in-linux
static char getch(){
	char buf=0;
	struct termios old={0};
	fflush(stdout);
	if(tcgetattr(0, &old)<0)
		perror("tcsetattr()");
	old.c_lflag&=~ICANON;
	old.c_lflag&=~ECHO;
	old.c_cc[VMIN]=1;
	old.c_cc[VTIME]=0;
	if(tcsetattr(0, TCSANOW, &old)<0)
		perror("tcsetattr ICANON");
	if(read(0,&buf,1)<0)
		perror("read()");
	old.c_lflag|=ICANON;
	old.c_lflag|=ECHO;
	if(tcsetattr(0, TCSADRAIN, &old)<0)
		perror ("tcsetattr ~ICANON");
	//printf("%c\n",buf);
	return buf;
}

string get_number_str(int *is_negative, int *is_real)
{
	char c;
	string str = "";
	int count = 0;
	int dot_count = 0;
	int sub_count = 0;

	do {
		c = getch();
		if ((c >= '0' && c <= '9')
			|| (count > 0 && c == '.' && dot_count == 0) 
			|| (count == 0 && c == '-' && sub_count == 0)
			|| c == '\n') {
			if (c == '\n') {
				if (count == 0) {
					continue;
				}
				break;
			} else if (c == '.') {
				if (is_real) {
					*is_real = 1;
				}
				dot_count++;
			} else if (c == '-') {
				if (is_negative) {
					*is_negative = 1;
				}
				sub_count++;
			}
			printf("%c", c);
		
			str += c;
			count++;
		}

	} while(1);
	return str;
}
