/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#include "hlx_server.hpp"

int main(int argc, char *argv[])
{
	int ret = -1;
	ChildBServer *cb = NULL;

	if (argc != 2) {
		ffloge("failed to start childB, argc need 2");
		goto finally;
	}

	if ((cb = new ChildBServer) == NULL) {
		ffloge("failed to new ChildBServer");
		goto finally;
	}

	if (cb->init(argv[1])) {
		fflog("failed to ChildBServer->init");
		goto finally;
	}

	if (cb->start()) {
		fflog("failed to ChildBServer->start");
		goto finally;
	}

	ret = 0;
finally:
	DO_DELETE(cb);
	return ret;
}
