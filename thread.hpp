/*
 * by Guan-Da Huang 2018-01 (deepkh@gmail.com)
 */
#ifndef _THREAD_H_
#define _THREAD_H_
#include <std.hpp>

/************************************
 * thread 
 ***********************************/
class Thread {
protected:
	pthread_t _thd;
	pthread_attr_t _attr;
	int _thd_status = 0;
public:
	~Thread();
	static Thread *create(void *(*f)(void *), void *arg);
};

enum {
	ModuleStatusStart = 0,
	ModuleStatusStop = -1,
};

class Module {
protected:
	string _name;
	int _status = ModuleStatusStop;
	char _log_buf[4096];
public:
	virtual ~Module() {;};
	virtual int init(string name);
	virtual int start();
	virtual int stop();
	virtual int status();
	virtual void log(const char *fmt, ...);
	virtual void log_noline(const char *fmt, ...);
	virtual void logln();
	virtual void loge(const char *fmt, ...);
};

enum {
	ChildProcessIsRunning = 0,
	ChildProcessWasStop = -1,
	ChildProcessWasWrong = -2,
};

class ChildProcess: public Module {
protected:
	pid_t _pid = 0;
public:
	virtual ~ChildProcess() {;};
	virtual int start();
	virtual int stop();
	virtual int status(int is_nonblock);
	virtual int status();
	virtual const char *exec_name() = 0;
	virtual const char **argvs() = 0;
	virtual posix_spawn_file_actions_t *file_actions();
};
#endif
