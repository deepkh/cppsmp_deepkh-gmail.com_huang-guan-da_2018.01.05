###############################################
#
# by Guan-Da Huang 2018-01 (deepkh@gmail.com)
#
###############################################

.PHONY: clean
.DEFAULT_GOAL := all

RM = rm -rf
CXX = $(TOOLCHAIN)g++
CFLAGS = -g -I. -O2 -Wall -D__STDC_CONSTANT_MACROS -D__STDC_FORMAT_MACROS
CXXFLAGS = $(CFLAGS) -std=c++11 
LDFLAGS = -lpthread -lrt

all: mainParent childA childB

mainParent: mainParent.o std.o shm.o thread.o hlx_client.o 
	$(CXX) -o $@ $^ $(LDFLAGS)
	
childA: childA.o std.o std.o shm.o thread.o hlx_client.o hlx_server.o
	$(CXX) -o $@ $^ $(LDFLAGS)
	
childB: childB.o std.o std.o shm.o thread.o hlx_client.o hlx_server.o
	$(CXX) -o $@ $^ $(LDFLAGS)
	
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $<

clean:
	$(RM) *.o mainParent childA childB
